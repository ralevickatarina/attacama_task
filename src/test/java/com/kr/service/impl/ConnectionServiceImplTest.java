package com.kr.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.kr.exceptions.ConnectionServiceException;
import com.kr.io.entity.ConnectionEntity;
import com.kr.io.repository.ConnectionRepository;
import com.kr.shared.Utils;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ErrorMessages;

class ConnectionServiceImplTest {

	@InjectMocks
	ConnectionServiceImpl connectionServiceImpl;
	
	@Mock
	ConnectionRepository connectionRepository;
	
	@Mock
	Utils utils;
	
	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	String connectionPublicId = "eSwyxfqQZMXlhiszlSAFosCcltvQzg";
	String encryptedPassword = "$2a$10$LuVREujG4MMaS3nTPj/VYuyeqn5yLiX9i9Q0DIJMJQXA4IYPWFFF2";
	ConnectionEntity connectionEntity;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		connectionEntity = new ConnectionEntity();
		connectionEntity.setId(1L);
		connectionEntity.setName("connection 1");
		connectionEntity.setPublicConnectionId(connectionPublicId);
		connectionEntity.setHostname("hostname 1");
		connectionEntity.setDatabaseName("dbName 1");
		connectionEntity.setPort("port 1");
		connectionEntity.setUsername("username 1");
		// connectionEntity.setEncryptedPassword(encryptedPassword);
	}

	@Test
	void testCreateConnection() {
		
		when (connectionRepository.findByName(anyString())).thenReturn(null);
		when(utils.generateConnectionId(anyInt())).thenReturn(connectionPublicId);
		when(bCryptPasswordEncoder.encode(anyString())).thenReturn(encryptedPassword);
		when(connectionRepository.save(any(ConnectionEntity.class))).thenReturn(connectionEntity);
		
		ConnectionDto connectionDto = new ConnectionDto();
		connectionDto.setName("connection 1");
		connectionDto.setHostname("hostname 1");
		connectionDto.setDatabaseName("dbName 1");
		connectionDto.setPort("port 1");
		connectionDto.setUsername("username 1");
		connectionDto.setPassword("password 1");
		
		ConnectionDto storedConnectionDetails = connectionServiceImpl.createConnection(connectionDto);
		
		assertNotNull(storedConnectionDetails);
		assertNotNull(storedConnectionDetails.getPublicConnectionId());
		assertNotNull(storedConnectionDetails.getId());
		
		assertEquals(connectionEntity.getName(), storedConnectionDetails.getName());
		assertEquals(connectionEntity.getUsername(), storedConnectionDetails.getUsername());
		
		verify(connectionRepository, times(1)).save(any(ConnectionEntity.class));
	}
	
	@Test
	final void testCreateConnection_ConnectionServiceException()
	{
		when(connectionRepository.findByName(anyString())).thenReturn(connectionEntity);
		
		ConnectionDto connectionDto = new ConnectionDto();
		connectionDto.setName("connection 1");
		connectionDto.setHostname("hostname 1");
		connectionDto.setDatabaseName("dbName 1");
		connectionDto.setPort("port 1");
		connectionDto.setUsername("username 1");
		connectionDto.setPassword("password 1");
		
		assertThrows(ConnectionServiceException.class,

				() -> {
					connectionServiceImpl.createConnection(connectionDto);
				},
				ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage()
		);
	}

	@Test
	void testGetConnectionById() {
		
		when(connectionRepository.findByPublicConnectionId(anyString())).thenReturn(connectionEntity);
		
		ConnectionDto connectionDto = connectionServiceImpl.getConnectionById(connectionPublicId);
		
		assertNotNull(connectionDto);
		assertEquals("connection 1", connectionDto.getName());
	}
	
	@Test
	final void testGetConnectionById_ConnectionServiceException() {
		
		when(connectionRepository.findByPublicConnectionId(anyString())).thenReturn(null);
		
		assertThrows(ConnectionServiceException.class, 
				() -> { connectionServiceImpl.getConnectionById(connectionPublicId); }, 
				ErrorMessages.NO_RECORD_FOUND.getErrorMessage()
				);
	}
}
