package com.kr.ui.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.kr.service.impl.ConnectionServiceImpl;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ConnectionRest;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class ConnectionControllerTest {
	
	@InjectMocks
	ConnectionController connectionController;
	
	@Mock
	ConnectionServiceImpl connectionServiceImpl;
	
	ConnectionDto connectionDto;
	
	final String USER_ID = "eSwyxfqQZMXlhiszlSAFosCcltvQzg";

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		connectionDto = new ConnectionDto();
		connectionDto.setName("connection 1");
		connectionDto.setHostname("hostname 1");
		connectionDto.setDatabaseName("dbName 1");
		connectionDto.setPort("port 1");
		connectionDto.setUsername("username 1");
		connectionDto.setPassword("password 1");
		connectionDto.setPublicConnectionId(USER_ID);
	}

	@Test
	void testGetConnection() {
		when(connectionServiceImpl.getConnectionById(anyString())).thenReturn(connectionDto);	
		    
	    ConnectionRest connectionRest = connectionController.getConnection(USER_ID);
		    
		assertNotNull(connectionRest);
	    assertEquals(USER_ID, connectionRest.getPublicConnectionId());
	    assertEquals(connectionDto.getName(), connectionRest.getName());
	    assertEquals(connectionDto.getUsername(), connectionRest.getUsername());
	}
}
