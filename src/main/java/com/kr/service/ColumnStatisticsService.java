package com.kr.service;

import com.kr.shared.dto.ConnectionDto;

public interface ColumnStatisticsService {
	String getColumnStatistics(ConnectionDto connectionDto, String password, String table, String column);
}
