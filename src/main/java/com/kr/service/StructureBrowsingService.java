package com.kr.service;

import java.util.List;

import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ColumnRest;
import com.kr.ui.model.response.SchemaRest;

public interface StructureBrowsingService {

	List<SchemaRest> getSchemas(ConnectionDto connectionDto, String password);
	String getTables(ConnectionDto connectionDto, String password);
	List<ColumnRest> getColumns(ConnectionDto connectionDto, String password, String tableName);
	String getTableData(ConnectionDto connectionDto, String password, String tableName);

}
