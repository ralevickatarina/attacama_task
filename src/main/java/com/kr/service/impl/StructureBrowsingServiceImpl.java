package com.kr.service.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kr.exceptions.ConnectionServiceException;
import com.kr.exceptions.StructureBrowsingException;
import com.kr.service.StructureBrowsingService;
import com.kr.shared.Utils;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ColumnRest;
import com.kr.ui.model.response.ErrorMessages;
import com.kr.ui.model.response.GeneralRest;
import com.kr.ui.model.response.SchemaRest;
import com.kr.ui.model.response.TableDataRest;
import com.kr.ui.model.response.TableRest;

import ch.qos.logback.core.joran.util.beans.BeanUtil;

@Service
public class StructureBrowsingServiceImpl implements StructureBrowsingService {

	@Override
	public List<SchemaRest> getSchemas(ConnectionDto connectionDto, String password) {

		DatabaseMetaData metadata = null;
		String connectionPassword = Utils.checkPassword(connectionDto, password);
		String URL = Utils.buildURL(connectionDto);
		List<SchemaRest> schemas = new ArrayList<>();
		SchemaRest schema;
		Connection connection = null;

		try {

			connection = DriverManager.getConnection(URL, connectionDto.getUsername(), connectionPassword);

			metadata = connection.getMetaData();
			ResultSet resultSet = metadata.getCatalogs();

			while (resultSet.next()) {
				schema = new SchemaRest();
				schema.setName(resultSet.getString("TABLE_CAT"));
				schemas.add(schema);
			}

			resultSet.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new StructureBrowsingException(ErrorMessages.COULD_NOT_CONNECT.getErrorMessage());
		} finally {

			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return schemas;
	}

	@Override
	public String getTables(ConnectionDto connectionDto, String password) {

		DatabaseMetaData metadata = null;
		String connectionPassword = Utils.checkPassword(connectionDto, password);
		String URL = Utils.buildURL(connectionDto) + "/" + connectionDto.getDatabaseName();

		List<GeneralRest> properties = new ArrayList<>();
		List<TableRest> tablesDetails = new ArrayList<>();

		JSONArray retrievedData = new JSONArray();
		JSONObject jsonObject;
		JSONObject returnValue = new org.json.JSONObject();
		Connection connection = null;
		
		try {

			connection = DriverManager.getConnection(URL, connectionDto.getUsername(),
					connectionPassword);
			
			metadata = connection.getMetaData();
			ResultSet resultSet = metadata.getTables(connectionDto.getDatabaseName(), null, null,
					new String[] { "TABLE" });

			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int columnsNumber = resultSetMetaData.getColumnCount();

			while (resultSet.next()) {

				jsonObject = new org.json.JSONObject();

				for (int i = 1; i <= columnsNumber; i++) {
					jsonObject.put(resultSetMetaData.getColumnName(i), resultSet.getString(i));
				}

				retrievedData.put(jsonObject);
			}

			returnValue.put(connectionDto.getDatabaseName(), retrievedData);

			resultSet.close();
			connection.close();
			
			return returnValue.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new StructureBrowsingException(ErrorMessages.COULD_NOT_CONNECT.getErrorMessage());
		} finally {
			
			try {
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<ColumnRest> getColumns(ConnectionDto connectionDto, String password, String tableName) {

		DatabaseMetaData metadata = null;
		String connectionPassword = Utils.checkPassword(connectionDto, password);
		String URL = Utils.buildURL(connectionDto) + "/" + connectionDto.getDatabaseName();

		List<GeneralRest> properties = new ArrayList<>();
		List<ColumnRest> columnsDetails = new ArrayList<>();
		Connection connection = null;

		try {
			
			connection = DriverManager.getConnection(URL, connectionDto.getUsername(), connectionPassword);
			metadata = connection.getMetaData();

			/*
			 * getting general column information
			 */
			ResultSet resultSet = metadata.getColumns(null, null, tableName, null);

			while (resultSet.next()) {

				properties = new ArrayList<>();

				String columnName = resultSet.getString("COLUMN_NAME");
				String columnType = resultSet.getString("TYPE_NAME");

				ColumnRest columnRest = new ColumnRest(columnName, columnType);

				properties.add(new GeneralRest("DATA_TYPE", resultSet.getString("DATA_TYPE")));
				properties.add(new GeneralRest("COLUMN_SIZE", resultSet.getString("COLUMN_SIZE")));
				properties.add(new GeneralRest("IS_NULLABLE", resultSet.getString("IS_NULLABLE")));
				properties.add(new GeneralRest("IS_AUTOINCREMENT", resultSet.getString("IS_AUTOINCREMENT")));

				columnRest.setProperties(properties);
				columnsDetails.add(columnRest);
			}

			resultSet.close();

			/*
			 * getting primary key column information
			 */
			resultSet = metadata.getPrimaryKeys(null, null, tableName);
			int index;

			while (resultSet.next()) {
				String columnName = resultSet.getString("COLUMN_NAME");
				ColumnRest columnRest = columnsDetails.stream()
						.filter(column -> column.getColumnName().equals(columnName)).findFirst().get();

				index = columnsDetails.indexOf(columnRest);
				properties = columnsDetails.get(index).getProperties();
				properties.add(new GeneralRest("PK_NAME", resultSet.getString("PK_NAME")));

				columnsDetails.get(columnsDetails.indexOf(columnRest)).setProperties(properties);
			}

			resultSet.close();

			/*
			 * getting foreign key column information
			 */

			resultSet = metadata.getImportedKeys(null, null, tableName);

			while (resultSet.next()) {

				String columnName = resultSet.getString("COLUMN_NAME");
				ColumnRest columnRest = columnsDetails.stream()
						.filter(column -> column.getColumnName().equals(columnName)).findFirst().get();

				index = columnsDetails.indexOf(columnRest);

				properties = columnsDetails.get(index).getProperties();
				properties.add(new GeneralRest("PKTABLE_NAME", resultSet.getString("PKTABLE_NAME")));
				properties.add(new GeneralRest("FKTABLE_NAME", resultSet.getString("FKTABLE_NAME")));
				properties.add(new GeneralRest("PKCOLUMN_NAME", resultSet.getString("PKCOLUMN_NAME")));
				properties.add(new GeneralRest("FKCOLUMN_NAME", resultSet.getString("FKCOLUMN_NAME")));

				columnsDetails.get(columnsDetails.indexOf(columnRest)).setProperties(properties);
			}

			resultSet.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new StructureBrowsingException(ErrorMessages.COULD_NOT_CONNECT.getErrorMessage());
		} finally {
			
			try {
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return columnsDetails;
	}

	public String getTableData(ConnectionDto connectionDto, String password, String tableName) {

		if (getTables(connectionDto, password).contains("\"TABLE_NAME\":\"" + tableName + "\"")) {

			JSONArray retrievedData = new JSONArray();
			JSONObject jsonObject;
			JSONObject returnValue = new org.json.JSONObject();

			String connectionPassword = Utils.checkPassword(connectionDto, password);
			String URL = Utils.buildURL(connectionDto) + "/" + connectionDto.getDatabaseName();
			Connection connection = null;
			
			try {

				connection = DriverManager.getConnection(URL, connectionDto.getUsername(),
						connectionPassword);
				String sqlSelectData = "SELECT * FROM " + tableName;
				PreparedStatement preparedStatement = connection.prepareStatement(sqlSelectData);

				ResultSet resultSet = preparedStatement.executeQuery(sqlSelectData);
				ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

				int columnsNumber = resultSetMetaData.getColumnCount();

				while (resultSet.next()) {
					jsonObject = new org.json.JSONObject();

					for (int i = 1; i <= columnsNumber; i++) {
						jsonObject.put(resultSetMetaData.getColumnName(i), resultSet.getString(i));
					}

					retrievedData.put(jsonObject);
				}

				returnValue.put(tableName, retrievedData);

				resultSet.close();
				connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
				throw new StructureBrowsingException(ErrorMessages.COULD_NOT_CONNECT.getErrorMessage());
			} finally {
				
				try {
					if (connection != null) connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			return returnValue.toString();
		} else {
			throw new StructureBrowsingException(ErrorMessages.TABLE_NOT_FOUND.getErrorMessage());
		}
	}
}
