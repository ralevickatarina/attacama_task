package com.kr.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.kr.io.repository.*;
import com.kr.exceptions.ConnectionServiceException;
import com.kr.io.entity.ConnectionEntity;
import com.kr.service.ConnectionService;
import com.kr.shared.Utils;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ErrorMessages;

@Service
public class ConnectionServiceImpl implements ConnectionService {

	@Autowired
	ConnectionRepository connectionRepository;
	
	@Autowired
	Utils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public ConnectionDto createConnection(ConnectionDto connection) {
		
		if (connectionRepository.findByName(connection.getName()) != null) throw new ConnectionServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());
		
		ConnectionEntity connectionEntity = new ConnectionEntity();
		BeanUtils.copyProperties(connection, connectionEntity);
		
		String publicConnectionId = utils.generateConnectionId(30);
		connectionEntity.setPublicConnectionId(publicConnectionId);
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		connectionEntity.setEncryptedPassword(passwordEncoder.encode(connection.getPassword()));
		
		ConnectionEntity storedConnectionDetails = connectionRepository.save(connectionEntity);
		
		ConnectionDto returnValue = new ConnectionDto();
		BeanUtils.copyProperties(storedConnectionDetails, returnValue);
		
		return returnValue;
	}

	@Override
	public ConnectionDto getConnectionById(String connectionId) {
		
		ConnectionDto returnValue = new ConnectionDto();
		ConnectionEntity connectionEntity = connectionRepository.findByPublicConnectionId(connectionId);
		
		if (connectionEntity == null) throw new ConnectionServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		BeanUtils.copyProperties(connectionEntity, returnValue);
		
		return returnValue;
	}

	@Override
	public ConnectionDto updateConnection(String connectionId, ConnectionDto connection) {
		
		ConnectionDto returnValue = new ConnectionDto();
		ConnectionEntity connectionEntity = connectionRepository.findByPublicConnectionId(connectionId);
		
		if (connectionEntity == null) throw new ConnectionServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		connectionEntity.setPort(connection.getPort());
		connectionEntity.setDatabaseName(connection.getDatabaseName());
		connectionEntity.setUsername(connection.getUsername());
		connectionEntity.setHostname(connection.getHostname());
		
		connectionEntity.setName(connection.getName());
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		connectionEntity.setEncryptedPassword(passwordEncoder.encode(connection.getPassword()));
		
		try {
			ConnectionEntity updatedConnectionDetails = connectionRepository.save(connectionEntity);
			BeanUtils.copyProperties(updatedConnectionDetails, returnValue);
		}
		catch (Exception ex) {
			throw new ConnectionServiceException(ErrorMessages.RECORD_ALREADY_EXISTS.getErrorMessage());
		}
		
		return returnValue;
	}

	@Override
	public void deleteConnection(String connectionId) {

		ConnectionEntity connectionEntity = connectionRepository.findByPublicConnectionId(connectionId);
		if (connectionEntity == null) throw new ConnectionServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		connectionRepository.delete(connectionEntity);		
	}

	@Override
	public List<ConnectionDto> getConnections(int page, int limit) {
		
		List<ConnectionDto> returnValue = new ArrayList<>();
		
		if (page > 0) page--;
		
		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<ConnectionEntity> connectionPage = connectionRepository.findAll(pageableRequest);
		
		List<ConnectionEntity> connections = connectionPage.getContent();
		
		for (ConnectionEntity connectionEntity : connections) {
			
			ConnectionDto connectionDto = new ConnectionDto();
			BeanUtils.copyProperties(connectionEntity, connectionDto);
			
			returnValue.add(connectionDto);
		}
		
		return returnValue;
	}
}
