package com.kr.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.kr.exceptions.ColumnStatisticsException;
import com.kr.exceptions.StructureBrowsingException;
import com.kr.service.ColumnStatisticsService;
import com.kr.shared.Utils;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ErrorMessages;

@Service
public class ColumnStatisticsServiceImpl implements ColumnStatisticsService {

	@Override
	public String getColumnStatistics(ConnectionDto connectionDto, String password, String tableName,
			String columnName) {

		String connectionPassword = Utils.checkPassword(connectionDto, password);
		String URL = Utils.buildURL(connectionDto) + "/" + connectionDto.getDatabaseName();

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(URL, connectionDto.getUsername(), connectionPassword);
			
			String sqlSelectData = "SELECT MAX(" + columnName + ") as maximum, MIN(" + columnName + ") as minimum, AVG(" + columnName + ") as average FROM " + tableName;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlSelectData);

			ResultSet resultSet = preparedStatement.executeQuery(sqlSelectData);
			JSONObject jsonObject = new JSONObject();

			resultSet.next();

			jsonObject.put("Maximum", resultSet.getString("maximum"));
			jsonObject.put("Minimum", resultSet.getString("minimum"));
			jsonObject.put("Average", resultSet.getString("average"));
			
			//preparedStatement.addBatch("set @ct := (select count(1) from " + tableName +");");
			//preparedStatement.addBatch("set @row_id := 0;");
			
			/*sqlSelectData = "select avg(" + columnName + ") as median "
					+ "from (select * from " + tableName +" order by " + columnName + ") randomName "
					+ "where (select @row_id := @row_id + 1)";
			
			preparedStatement.executeBatch();
			resultSet = preparedStatement.executeQuery(sqlSelectData);
			System.out.println(resultSet.getFetchSize());
			resultSet.next();
			jsonObject.put("Median", resultSet.getString("median"));
			*/
			
			preparedStatement.close();
			resultSet.close();
			connection.close();

			return jsonObject.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			// e.getMessage()
			throw new StructureBrowsingException(e.getMessage());
		} finally {
			
			try {
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
