package com.kr.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.kr.exceptions.StructureBrowsingException;
import com.kr.exceptions.TableStatisticsException;
import com.kr.service.TableStatisticsService;
import com.kr.shared.Utils;
import com.kr.shared.dto.ConnectionDto;

@Service
public class TableStatisticsServiceImpl implements TableStatisticsService {

	@Override
	public String getTableStatistics(ConnectionDto connectionDto, String password, String table) {
		
		String connectionPassword = Utils.checkPassword(connectionDto, password);
		String URL = Utils.buildURL(connectionDto) + "/" + connectionDto.getDatabaseName();
		Connection connection = null;

		try {
			
			connection = DriverManager.getConnection(URL, connectionDto.getUsername(), connectionPassword);
			
			String sqlSelectData = "SELECT count(*) as rows FROM " + table;
			PreparedStatement preparedStatement = connection.prepareStatement(sqlSelectData);
			
			ResultSet resultSet = preparedStatement.executeQuery(sqlSelectData);
			JSONObject jsonObject = new JSONObject();
			
			resultSet.next();
			jsonObject.put("Number of Records", resultSet.getString("rows"));
			
			sqlSelectData = "SELECT count(*) as columns FROM information_schema.columns WHERE table_name = ?";
			preparedStatement = connection.prepareStatement(sqlSelectData);
			preparedStatement.setString(1, table);

			resultSet = preparedStatement.executeQuery();
			resultSet.next();
				
			jsonObject.put("Number of Attributes", resultSet.getString("columns"));
			
			preparedStatement.close();
			resultSet.close();
			connection.close();
			
			return jsonObject.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new TableStatisticsException(e.getMessage());
		} finally {
			
			try {
				if (connection != null) connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
