package com.kr.service;

import java.util.List;

import com.kr.shared.dto.ConnectionDto;

public interface ConnectionService {

	ConnectionDto createConnection(ConnectionDto connection);
	ConnectionDto getConnectionById(String connectionId);
	ConnectionDto updateConnection(String connectionId, ConnectionDto connection);
	void deleteConnection(String connectionId);
	List<ConnectionDto> getConnections(int page, int limit);
}
