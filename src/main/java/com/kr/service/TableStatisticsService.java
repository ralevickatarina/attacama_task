package com.kr.service;

import com.kr.shared.dto.ConnectionDto;

public interface TableStatisticsService {

	String getTableStatistics(ConnectionDto connectionDto, String password, String table);
}
