package com.kr;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class AtaccamaHomeworkApplication {
	
	private static final String URL = "jdbc:mysql://localhost:3308/ataccama";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

	public static void main(String[] args) {
		SpringApplication.run(AtaccamaHomeworkApplication.class, args);
		
		/* try (Connection connection =
                 DriverManager.getConnection(URL, USERNAME, PASSWORD)) {

            DatabaseMetaData metadata = connection.getMetaData();
            ResultSet resultSet =
                metadata.getColumns(null, null, "connections", null);

            while (resultSet.next()) {
                String name = resultSet.getString("COLUMN_NAME");
                String type = resultSet.getString("TYPE_NAME");
                int size = resultSet.getInt("COLUMN_SIZE");

                System.out.println("Column name: [" + name + "]; " +
                    "type: [" + type + "]; size: [" + size + "]");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
