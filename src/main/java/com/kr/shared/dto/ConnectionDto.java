package com.kr.shared.dto;

import java.io.Serializable;

public class ConnectionDto implements Serializable {

	private static final long serialVersionUID = -709288112698190140L;
	
	private long id;
	private String publicConnectionId;
	private String name;
	private String hostname;
	private String port;
	private String databaseName;
	private String username;
	private String password;
	private String encryptedPassword;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPublicConnectionId() {
		return publicConnectionId;
	}

	public void setPublicConnectionId(String publicConnectionId) {
		this.publicConnectionId = publicConnectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
}
