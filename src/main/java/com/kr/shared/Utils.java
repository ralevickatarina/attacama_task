package com.kr.shared;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.kr.exceptions.StructureBrowsingException;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ErrorMessages;

@Component
public class Utils {

	private final Random RANDOM = new SecureRandom();
    private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    
    public String generateConnectionId(int length) {
        return generateRandomString(length);
    }
    
    private String generateRandomString(int length) {
        StringBuilder returnValue = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }

        return new String(returnValue);
    }
    
    public static String buildURL(ConnectionDto connectionDto) {
		// jdbc:mysql://localhost:3308
		return "jdbc:mysql://" + connectionDto.getHostname() + ":" + connectionDto.getPort();
	}

    public static String checkPassword(ConnectionDto connectionDto, String password) {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if (passwordEncoder.matches(password, connectionDto.getEncryptedPassword())) {
			return password;
		} else {
			throw new StructureBrowsingException(ErrorMessages.INCORRECT_PASSWORD.getErrorMessage());
		}
	}
}
