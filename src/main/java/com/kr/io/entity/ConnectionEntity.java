package com.kr.io.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "connections")
public class ConnectionEntity implements Serializable {

	private static final long serialVersionUID = -4564789457376946085L;

	@Id
	@GeneratedValue
	private long id;

	// alphanumeric public connection id, sent as a part of response
	@Column(nullable = false)
	private String publicConnectionId;

	@Column(nullable = false, length = 30, unique = true)
	private String name;

	@Column(nullable = false, length = 50)
	private String hostname;

	@Column(nullable = false, length = 5)
	private String port;

	@Column(nullable = false, length = 50)
	private String databaseName;

	@Column(nullable = false, length = 50)
	private String username;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPublicConnectionId() {
		return publicConnectionId;
	}

	public void setPublicConnectionId(String publicConnectionId) {
		this.publicConnectionId = publicConnectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@Column(nullable = false)
	private String encryptedPassword;
}
