package com.kr.io.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.kr.io.entity.ConnectionEntity;

@Repository
public interface ConnectionRepository extends PagingAndSortingRepository<ConnectionEntity, Long> {

	ConnectionEntity findByName(String name);
	ConnectionEntity findByPublicConnectionId(String publicConnectionId);
}
