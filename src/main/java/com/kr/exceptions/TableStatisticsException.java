package com.kr.exceptions;

public class TableStatisticsException extends RuntimeException {

	private static final long serialVersionUID = 2548775509171435607L;

	public TableStatisticsException(String message)
	{
		super(message);
	}
}
