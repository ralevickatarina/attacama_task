package com.kr.exceptions;

public class ColumnStatisticsException extends RuntimeException {

	private static final long serialVersionUID = 3748771109171435607L;

	public ColumnStatisticsException(String message)
	{
		super(message);
	}
}
