package com.kr.exceptions;

public class ConnectionServiceException extends RuntimeException {

	private static final long serialVersionUID = 1348771109171435607L;

	public ConnectionServiceException(String message)
	{
		super(message);
	}
}
