package com.kr.exceptions;

public class StructureBrowsingException extends RuntimeException {

	private static final long serialVersionUID = 2548771109171435607L;

	public StructureBrowsingException(String message)
	{
		super(message);
	}
}
