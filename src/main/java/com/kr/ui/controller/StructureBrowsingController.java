package com.kr.ui.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kr.service.ConnectionService;
import com.kr.service.StructureBrowsingService;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ColumnRest;
import com.kr.ui.model.response.ConnectionRest;
import com.kr.ui.model.response.GeneralRest;
import com.kr.ui.model.response.SchemaRest;
import com.kr.ui.model.response.TableDataRest;
import com.kr.ui.model.response.TableRest;

@RestController
@RequestMapping("/listing") //http://localhost:8080/connections
public class StructureBrowsingController {

	@Autowired
	StructureBrowsingService browsingService;
	
	@Autowired
	ConnectionService connectionService;
	
	@GetMapping(path="/schemas/{id}/{password}")
	public List<SchemaRest> getSchemas(@PathVariable String id, @PathVariable String password) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		return browsingService.getSchemas(connectionDto, password);
	}
	
	@GetMapping(path="/tables/{id}/{password}")
	public String getTables(@PathVariable String id, @PathVariable String password) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		return browsingService.getTables(connectionDto, password);
	}
	
	@GetMapping(path="/columns/{id}/{password}/{table}")
	public List<ColumnRest> getColumns(@PathVariable String id, @PathVariable String password, @PathVariable String table) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		return browsingService.getColumns(connectionDto, password, table);
	}
	
	@GetMapping(path="/data/{id}/{password}/{table}")
	public String getTableData(@PathVariable String id, @PathVariable String password, @PathVariable String table) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		return browsingService.getTableData(connectionDto, password, table);
	}
}
