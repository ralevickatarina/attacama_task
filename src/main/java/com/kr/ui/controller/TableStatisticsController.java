package com.kr.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kr.service.ColumnStatisticsService;
import com.kr.service.ConnectionService;
import com.kr.service.TableStatisticsService;
import com.kr.shared.dto.ConnectionDto;

@RestController
@RequestMapping("/tableStatistic") //http://localhost:8080/tableStatistic
public class TableStatisticsController {

	@Autowired
	TableStatisticsService tableStatisticsService;
	
	@Autowired
	ConnectionService connectionService;
	
	@GetMapping(path="/{id}/{password}/{table}")
	public String getColumnStatistics(@PathVariable String id, @PathVariable String password, @PathVariable String table) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		return tableStatisticsService.getTableStatistics(connectionDto, password, table);
	}
}
