package com.kr.ui.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kr.service.ColumnStatisticsService;
import com.kr.service.ConnectionService;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.response.ConnectionRest;

@RestController
@RequestMapping("/columnStatistic") //http://localhost:8080/columnStatistic
public class ColumnStatisticsController {

	@Autowired
	ColumnStatisticsService columnStatisticsService;
	
	@Autowired
	ConnectionService connectionService;
	
	@GetMapping(path="/{id}/{password}/{table}/{column}")
	public String getColumnStatistics(@PathVariable String id, @PathVariable String password, @PathVariable String table, @PathVariable String column) {
		
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		return columnStatisticsService.getColumnStatistics(connectionDto, password, table, column);
	}
}
