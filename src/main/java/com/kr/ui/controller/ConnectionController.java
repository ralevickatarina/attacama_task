package com.kr.ui.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kr.exceptions.ConnectionServiceException;
import com.kr.service.ConnectionService;
import com.kr.shared.dto.ConnectionDto;
import com.kr.ui.model.request.ConnectionDetailsRequestModel;
import com.kr.ui.model.response.ConnectionRest;
import com.kr.ui.model.response.ErrorMessages;
import com.kr.ui.model.response.OperationStatusModel;

@RestController
@RequestMapping("/connections") //http://localhost:8080/connections
public class ConnectionController {
	
	@Autowired
	ConnectionService connectionService;
	
	@GetMapping(path="/{id}")
	public ConnectionRest getConnection(@PathVariable String id) {
		
		ConnectionRest returnValue = new ConnectionRest();
		ConnectionDto connectionDto = connectionService.getConnectionById(id);
		
		BeanUtils.copyProperties(connectionDto, returnValue);
		
		return returnValue;
	}
	
	@GetMapping
	public List<ConnectionRest> getConnections(@RequestParam(value="page", defaultValue = "0") int page, @RequestParam(value="limit", defaultValue = "25") int limit) {
		
		List<ConnectionRest> returnValue = new ArrayList<>();
		List<ConnectionDto> connections = connectionService.getConnections(page, limit);
		
		for (ConnectionDto connectionDto : connections) {
			
			ConnectionRest connectionModel = new ConnectionRest();
			BeanUtils.copyProperties(connectionDto, connectionModel);
			
			returnValue.add(connectionModel);
		}
		
		return returnValue;
	}
	
	@PostMapping
	public ConnectionRest createConnection(@RequestBody ConnectionDetailsRequestModel connectionDetails) throws Exception {
		
		ConnectionRest returnValue = new ConnectionRest();
		
		ConnectionDto connectionDto = new ConnectionDto();
		BeanUtils.copyProperties(connectionDetails, connectionDto);
		
		ConnectionDto createdConnection = connectionService.createConnection(connectionDto);
		BeanUtils.copyProperties(createdConnection, returnValue);
		
		return returnValue;
	}
	
	@PutMapping(path="/{id}")
	public ConnectionRest updateConnection(@PathVariable String id, @RequestBody ConnectionDetailsRequestModel connectionDetails) {
		
		ConnectionRest returnValue = new ConnectionRest();
		
		ConnectionDto connectionDto = new ConnectionDto();
		BeanUtils.copyProperties(connectionDetails, connectionDto);
		
		ConnectionDto updatedConnection = connectionService.updateConnection(id, connectionDto);
		BeanUtils.copyProperties(updatedConnection, returnValue);
		
		return returnValue;
	}
	
	@DeleteMapping(path="/{id}")
	public OperationStatusModel deleteConnection(@PathVariable String id) {
		
		OperationStatusModel operationStatusModel = new OperationStatusModel();
		
		operationStatusModel.setOperationName("DELETE");
		connectionService.deleteConnection(id);		
		operationStatusModel.setOperationResult("SUCCESS");
		
		return operationStatusModel;
	}
}
