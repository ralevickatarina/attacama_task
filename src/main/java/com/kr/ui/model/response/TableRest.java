package com.kr.ui.model.response;

import java.util.List;

public class TableRest {

	private String name;
	private String remarks;
	private List<GeneralRest> properties;

	public TableRest(String name, String remarks) {
		super();
		this.name = name;
		this.remarks = remarks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<GeneralRest> getProperties() {
		return properties;
	}

	public void setProperties(List<GeneralRest> properties) {
		this.properties = properties;
	}

}
