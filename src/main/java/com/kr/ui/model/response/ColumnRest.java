package com.kr.ui.model.response;

import java.util.List;

public class ColumnRest {

	private String columnName;
	private String columnType;

	private List<GeneralRest> properties;

	public ColumnRest(String columnName, String columnType) {
		super();
		this.columnName = columnName;
		this.columnType = columnType;
	}
	
	public void addProperty(GeneralRest generalRest) {
		this.properties.add(generalRest);
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public List<GeneralRest> getProperties() {
		return properties;
	}

	public void setProperties(List<GeneralRest> properties) {
		this.properties = properties;
	}
}
