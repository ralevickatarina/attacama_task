package com.kr.ui.model.response;

public class ConnectionRest {

	private String publicConnectionId;
	private String name;
	private String hostname;
	private String port;
	private String databaseName;
	private String username;

	public String getPublicConnectionId() {
		return publicConnectionId;
	}

	public void setPublicConnectionId(String publicConnectionId) {
		this.publicConnectionId = publicConnectionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
